window.skins={};
                function __extends(d, b) {
                    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
                        function __() {
                            this.constructor = d;
                        }
                    __.prototype = b.prototype;
                    d.prototype = new __();
                };
                window.generateEUI = {};
                generateEUI.paths = {};
                generateEUI.styles = undefined;
                generateEUI.skins = {"eui.Button":"resource/eui_skins/ButtonSkin.exml","eui.CheckBox":"resource/eui_skins/CheckBoxSkin.exml","eui.HScrollBar":"resource/eui_skins/HScrollBarSkin.exml","eui.HSlider":"resource/eui_skins/HSliderSkin.exml","eui.Panel":"resource/eui_skins/PanelSkin.exml","eui.TextInput":"resource/eui_skins/TextInputSkin.exml","eui.ProgressBar":"resource/eui_skins/ProgressBarSkin.exml","eui.RadioButton":"resource/eui_skins/RadioButtonSkin.exml","eui.Scroller":"resource/eui_skins/ScrollerSkin.exml","eui.ToggleSwitch":"resource/eui_skins/ToggleSwitchSkin.exml","eui.VScrollBar":"resource/eui_skins/VScrollBarSkin.exml","eui.VSlider":"resource/eui_skins/VSliderSkin.exml","eui.ItemRenderer":"resource/eui_skins/ItemRendererSkin.exml","Game":"resource/scene/Game.exml","UILogin":"resource/scene/UILogin.exml","UIChrSel":"resource/scene/UIChrSel.exml"};generateEUI.paths['resource/euix/ImageButton.exml'] = window.ImageButtonSkin = (function (_super) {
	__extends(ImageButtonSkin, _super);
	function ImageButtonSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay","iconDisplay","src"];
		
		this.currentState = "up";
		this.minHeight = 50;
		this.minWidth = 100;
		this.elementsContent = [];
		this._Image1_i();
		
		this.labelDisplay_i();
		
		this.iconDisplay_i();
		
		this.src_i();
		
		this.states = [
			new eui.State ("up",
				[
					new eui.AddItems("_Image1","",0,""),
					new eui.AddItems("labelDisplay","",1,""),
					new eui.AddItems("iconDisplay","",1,""),
					new eui.AddItems("src","",1,"")
				])
			,
			new eui.State ("down",
				[
					new eui.AddItems("_Image1","",0,""),
					new eui.AddItems("labelDisplay","",1,""),
					new eui.AddItems("iconDisplay","",1,""),
					new eui.AddItems("src","",1,""),
					new eui.SetProperty("_Image1","source","button_down_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.AddItems("_Image1","",0,""),
					new eui.AddItems("labelDisplay","",1,""),
					new eui.AddItems("iconDisplay","",1,""),
					new eui.AddItems("src","",1,""),
					new eui.SetProperty("_Image1","alpha",0.5)
				])
		];
	}
	var _proto = ImageButtonSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.anchorOffsetX = 0;
		t.percentHeight = 100;
		t.horizontalCenter = 0;
		t.scale9Grid = new egret.Rectangle(22,23,125,3);
		t.source = "btn_relive_png";
		t.verticalCenter = 0;
		t.width = 316;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Microsoft YaHei";
		t.percentHeight = 100;
		t.horizontalCenter = 0.5;
		t.size = 32;
		t.text = "";
		t.textAlign = "center";
		t.textColor = 0xFFFFFF;
		t.verticalAlign = "middle";
		t.verticalCenter = -5;
		t.width = 316;
		return t;
	};
	_proto.iconDisplay_i = function () {
		var t = new eui.Image();
		this.iconDisplay = t;
		t.anchorOffsetY = -1;
		t.percentHeight = 100;
		t.horizontalCenter = 0.5;
		t.source = "";
		t.verticalCenter = 0.5;
		t.width = 316;
		return t;
	};
	_proto.src_i = function () {
		var t = new eui.Image();
		this.src = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillMode = "clip";
		t.percentHeight = 100;
		t.horizontalCenter = 0.5;
		t.source = "handle_png";
		t.verticalCenter = -4.5;
		t.width = 316;
		return t;
	};
	return ImageButtonSkin;
})(eui.Skin);generateEUI.paths['resource/euix/LoginButton.exml'] = window.LoginButton = (function (_super) {
	__extends(LoginButton, _super);
	function LoginButton() {
		_super.call(this);
		this.skinParts = ["labelDisplay","iconDisplay","src"];
		
		this.currentState = "up";
		this.minHeight = 50;
		this.minWidth = 100;
		this.elementsContent = [];
		this._Image1_i();
		
		this.labelDisplay_i();
		
		this.iconDisplay_i();
		
		this.src_i();
		
		this.states = [
			new eui.State ("up",
				[
					new eui.AddItems("_Image1","",0,""),
					new eui.AddItems("labelDisplay","",1,""),
					new eui.AddItems("iconDisplay","",1,""),
					new eui.AddItems("src","",1,"")
				])
			,
			new eui.State ("down",
				[
					new eui.AddItems("_Image1","",0,""),
					new eui.AddItems("labelDisplay","",1,""),
					new eui.AddItems("iconDisplay","",1,""),
					new eui.AddItems("src","",1,""),
					new eui.SetProperty("_Image1","source","button_down_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.AddItems("_Image1","",0,""),
					new eui.AddItems("labelDisplay","",1,""),
					new eui.AddItems("iconDisplay","",1,""),
					new eui.AddItems("src","",1,""),
					new eui.SetProperty("_Image1","alpha",0.5)
				])
		];
	}
	var _proto = LoginButton.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.anchorOffsetX = 0;
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.width = 121;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.bottom = 8;
		t.fontFamily = "Microsoft YaHei";
		t.left = 8;
		t.right = 8;
		t.size = 32;
		t.text = "";
		t.textAlign = "center";
		t.textColor = 0xFFFFFF;
		t.top = 8;
		t.verticalAlign = "middle";
		return t;
	};
	_proto.iconDisplay_i = function () {
		var t = new eui.Image();
		this.iconDisplay = t;
		t.horizontalCenter = 0;
		t.source = "";
		t.verticalCenter = 0;
		return t;
	};
	_proto.src_i = function () {
		var t = new eui.Image();
		this.src = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillMode = "scale";
		t.height = 40.8;
		t.horizontalCenter = 2.5;
		t.source = "0";
		t.verticalCenter = -3.5;
		t.width = 43.67;
		return t;
	};
	return LoginButton;
})(eui.Skin);generateEUI.paths['resource/eui_skins/AnimationSkin.exml'] = window.skins.AnimationSkin = (function (_super) {
	__extends(AnimationSkin, _super);
	function AnimationSkin() {
		_super.call(this);
		this.skinParts = ["tweenGroup","image"];
		
		this.height = 300;
		this.width = 400;
		this.tweenGroup_i();
		this.elementsContent = [this.image_i()];
		
		eui.Binding.$bindProperties(this, ["image"],[0],this._TweenItem1,"target");
		eui.Binding.$bindProperties(this, [0.1],[],this._Object1,"alpha");
		eui.Binding.$bindProperties(this, [1],[],this._Object2,"alpha");
	}
	var _proto = AnimationSkin.prototype;

	_proto.tweenGroup_i = function () {
		var t = new egret.tween.TweenGroup();
		this.tweenGroup = t;
		t.items = [this._TweenItem1_i()];
		return t;
	};
	_proto._TweenItem1_i = function () {
		var t = new egret.tween.TweenItem();
		this._TweenItem1 = t;
		t.paths = [this._To1_i(),this._To2_i()];
		return t;
	};
	_proto._To1_i = function () {
		var t = new egret.tween.To();
		t.duration = 450;
		t.props = this._Object1_i();
		return t;
	};
	_proto._Object1_i = function () {
		var t = {};
		this._Object1 = t;
		return t;
	};
	_proto._To2_i = function () {
		var t = new egret.tween.To();
		t.duration = 550;
		t.props = this._Object2_i();
		return t;
	};
	_proto._Object2_i = function () {
		var t = {};
		this._Object2 = t;
		return t;
	};
	_proto.image_i = function () {
		var t = new eui.Image();
		this.image = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillMode = "clip";
		t.height = 51;
		t.source = "resource/profile/btnEquip.png";
		t.width = 169;
		t.x = 77;
		t.y = 99.5;
		return t;
	};
	return AnimationSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ButtonSkin.exml'] = window.skins.ButtonSkin = (function (_super) {
	__extends(ButtonSkin, _super);
	function ButtonSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay","iconDisplay"];
		
		this.minHeight = 50;
		this.minWidth = 100;
		this.elementsContent = [this._Image1_i(),this.labelDisplay_i(),this.iconDisplay_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","button_down_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
		];
	}
	var _proto = ButtonSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.anchorOffsetX = 0;
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Microsoft YaHei";
		t.percentHeight = 80;
		t.horizontalCenter = 0;
		t.size = 32;
		t.text = "";
		t.textAlign = "center";
		t.textColor = 0xFFFFFF;
		t.verticalCenter = 0;
		t.percentWidth = 80;
		return t;
	};
	_proto.iconDisplay_i = function () {
		var t = new eui.Image();
		this.iconDisplay = t;
		t.percentHeight = 100;
		t.horizontalCenter = 0;
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	return ButtonSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/CheckBoxSkin.exml'] = window.skins.CheckBoxSkin = (function (_super) {
	__extends(CheckBoxSkin, _super);
	function CheckBoxSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.elementsContent = [this._Group1_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","alpha",0.7)
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_up_png")
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_down_png")
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_disabled_png")
				])
		];
	}
	var _proto = CheckBoxSkin.prototype;

	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.percentHeight = 100;
		t.percentWidth = 100;
		t.layout = this._HorizontalLayout1_i();
		t.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		return t;
	};
	_proto._HorizontalLayout1_i = function () {
		var t = new eui.HorizontalLayout();
		t.verticalAlign = "middle";
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.alpha = 1;
		t.fillMode = "scale";
		t.source = "checkbox_unselect_png";
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		return t;
	};
	return CheckBoxSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/HScrollBarSkin.exml'] = window.skins.HScrollBarSkin = (function (_super) {
	__extends(HScrollBarSkin, _super);
	function HScrollBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb"];
		
		this.minHeight = 8;
		this.minWidth = 20;
		this.elementsContent = [this.thumb_i()];
	}
	var _proto = HScrollBarSkin.prototype;

	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.height = 8;
		t.scale9Grid = new egret.Rectangle(3,3,2,2);
		t.source = "roundthumb_png";
		t.verticalCenter = 0;
		t.width = 30;
		return t;
	};
	return HScrollBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/HSliderSkin.exml'] = window.skins.HSliderSkin = (function (_super) {
	__extends(HSliderSkin, _super);
	function HSliderSkin() {
		_super.call(this);
		this.skinParts = ["track","thumb"];
		
		this.minHeight = 8;
		this.minWidth = 20;
		this.elementsContent = [this.track_i(),this.thumb_i()];
	}
	var _proto = HSliderSkin.prototype;

	_proto.track_i = function () {
		var t = new eui.Image();
		this.track = t;
		t.height = 6;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_sb_png";
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.source = "thumb_png";
		t.verticalCenter = 0;
		return t;
	};
	return HSliderSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ItemRendererSkin.exml'] = window.skins.ItemRendererSkin = (function (_super) {
	__extends(ItemRendererSkin, _super);
	function ItemRendererSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.minHeight = 50;
		this.minWidth = 100;
		this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","button_down_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
		];
		
		eui.Binding.$bindProperties(this, ["hostComponent.data"],[0],this.labelDisplay,"text");
	}
	var _proto = ItemRendererSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.bottom = 8;
		t.fontFamily = "Tahoma";
		t.left = 8;
		t.right = 8;
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0xFFFFFF;
		t.top = 8;
		t.verticalAlign = "middle";
		return t;
	};
	return ItemRendererSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/PanelSkin.exml'] = window.skins.PanelSkin = (function (_super) {
	__extends(PanelSkin, _super);
	function PanelSkin() {
		_super.call(this);
		this.skinParts = ["titleDisplay","moveArea","closeButton"];
		
		this.minHeight = 230;
		this.minWidth = 450;
		this.elementsContent = [this._Image1_i(),this.moveArea_i(),this.closeButton_i()];
	}
	var _proto = PanelSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.bottom = 0;
		t.left = 0;
		t.right = 0;
		t.scale9Grid = new egret.Rectangle(2,2,12,12);
		t.source = "border_png";
		t.top = 0;
		return t;
	};
	_proto.moveArea_i = function () {
		var t = new eui.Group();
		this.moveArea = t;
		t.height = 45;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		t.elementsContent = [this._Image2_i(),this.titleDisplay_i()];
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.bottom = 0;
		t.left = 0;
		t.right = 0;
		t.source = "header_png";
		t.top = 0;
		return t;
	};
	_proto.titleDisplay_i = function () {
		var t = new eui.Label();
		this.titleDisplay = t;
		t.fontFamily = "Tahoma";
		t.left = 15;
		t.right = 5;
		t.size = 20;
		t.textColor = 0xFFFFFF;
		t.verticalCenter = 0;
		t.wordWrap = false;
		return t;
	};
	_proto.closeButton_i = function () {
		var t = new eui.Button();
		this.closeButton = t;
		t.bottom = 5;
		t.horizontalCenter = 0;
		t.label = "close";
		return t;
	};
	return PanelSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ProgressBarSkin.exml'] = window.skins.ProgressBarSkin = (function (_super) {
	__extends(ProgressBarSkin, _super);
	function ProgressBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb","labelDisplay"];
		
		this.minHeight = 18;
		this.minWidth = 30;
		this.elementsContent = [this._Image1_i(),this.thumb_i(),this.labelDisplay_i()];
	}
	var _proto = ProgressBarSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_pb_png";
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.percentHeight = 100;
		t.source = "thumb_pb_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.horizontalCenter = 0;
		t.size = 15;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		t.verticalCenter = 0;
		return t;
	};
	return ProgressBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/RadioButtonSkin.exml'] = window.skins.RadioButtonSkin = (function (_super) {
	__extends(RadioButtonSkin, _super);
	function RadioButtonSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.elementsContent = [this._Group1_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","alpha",0.7)
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_up_png")
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_down_png")
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_disabled_png")
				])
		];
	}
	var _proto = RadioButtonSkin.prototype;

	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.percentHeight = 100;
		t.percentWidth = 100;
		t.layout = this._HorizontalLayout1_i();
		t.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		return t;
	};
	_proto._HorizontalLayout1_i = function () {
		var t = new eui.HorizontalLayout();
		t.verticalAlign = "middle";
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.alpha = 1;
		t.fillMode = "scale";
		t.source = "radiobutton_unselect_png";
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		return t;
	};
	return RadioButtonSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ScrollerSkin.exml'] = window.skins.ScrollerSkin = (function (_super) {
	__extends(ScrollerSkin, _super);
	function ScrollerSkin() {
		_super.call(this);
		this.skinParts = ["horizontalScrollBar","verticalScrollBar"];
		
		this.minHeight = 20;
		this.minWidth = 20;
		this.elementsContent = [this.horizontalScrollBar_i(),this.verticalScrollBar_i()];
	}
	var _proto = ScrollerSkin.prototype;

	_proto.horizontalScrollBar_i = function () {
		var t = new eui.HScrollBar();
		this.horizontalScrollBar = t;
		t.bottom = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.verticalScrollBar_i = function () {
		var t = new eui.VScrollBar();
		this.verticalScrollBar = t;
		t.percentHeight = 100;
		t.right = 0;
		return t;
	};
	return ScrollerSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/TextInputSkin.exml'] = window.skins.TextInputSkin = (function (_super) {
	__extends(TextInputSkin, _super);
	function TextInputSkin() {
		_super.call(this);
		this.skinParts = ["textDisplay","promptDisplay"];
		
		this.minHeight = 40;
		this.minWidth = 300;
		this.elementsContent = [this._Image1_i(),this._Rect1_i(),this.textDisplay_i()];
		this.promptDisplay_i();
		
		this.states = [
			new eui.State ("normal",
				[
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("textDisplay","textColor",0xff0000)
				])
			,
			new eui.State ("normalWithPrompt",
				[
					new eui.AddItems("promptDisplay","",1,"")
				])
			,
			new eui.State ("disabledWithPrompt",
				[
					new eui.AddItems("promptDisplay","",1,"")
				])
		];
	}
	var _proto = TextInputSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.fillAlpha = 0.1;
		t.fillColor = 0x510505;
		t.percentHeight = 100;
		t.strokeAlpha = 0.5;
		t.percentWidth = 100;
		return t;
	};
	_proto.textDisplay_i = function () {
		var t = new eui.EditableText();
		this.textDisplay = t;
		t.height = 24;
		t.left = "10";
		t.right = "10";
		t.size = 20;
		t.textColor = 0x000000;
		t.verticalCenter = "0";
		t.percentWidth = 100;
		return t;
	};
	_proto.promptDisplay_i = function () {
		var t = new eui.Label();
		this.promptDisplay = t;
		t.height = 24;
		t.left = 10;
		t.right = 10;
		t.size = 16;
		t.text = "name";
		t.textColor = 0xffffff;
		t.touchEnabled = false;
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	return TextInputSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ToggleSwitchSkin.exml'] = window.skins.ToggleSwitchSkin = (function (_super) {
	__extends(ToggleSwitchSkin, _super);
	function ToggleSwitchSkin() {
		_super.call(this);
		this.skinParts = [];
		
		this.elementsContent = [this._Image1_i(),this._Image2_i()];
		this.states = [
			new eui.State ("up",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
		];
	}
	var _proto = ToggleSwitchSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.source = "on_png";
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		this._Image2 = t;
		t.horizontalCenter = -18;
		t.source = "handle_png";
		t.verticalCenter = 0;
		return t;
	};
	return ToggleSwitchSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/VScrollBarSkin.exml'] = window.skins.VScrollBarSkin = (function (_super) {
	__extends(VScrollBarSkin, _super);
	function VScrollBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb"];
		
		this.minHeight = 20;
		this.minWidth = 8;
		this.elementsContent = [this.thumb_i()];
	}
	var _proto = VScrollBarSkin.prototype;

	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.height = 30;
		t.horizontalCenter = 0;
		t.scale9Grid = new egret.Rectangle(3,3,2,2);
		t.source = "roundthumb_png";
		t.width = 8;
		return t;
	};
	return VScrollBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/VSliderSkin.exml'] = window.skins.VSliderSkin = (function (_super) {
	__extends(VSliderSkin, _super);
	function VSliderSkin() {
		_super.call(this);
		this.skinParts = ["track","thumb"];
		
		this.minHeight = 30;
		this.minWidth = 25;
		this.elementsContent = [this.track_i(),this.thumb_i()];
	}
	var _proto = VSliderSkin.prototype;

	_proto.track_i = function () {
		var t = new eui.Image();
		this.track = t;
		t.percentHeight = 100;
		t.horizontalCenter = 0;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_png";
		t.width = 7;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.horizontalCenter = 0;
		t.source = "thumb_png";
		return t;
	};
	return VSliderSkin;
})(eui.Skin);generateEUI.paths['resource/scene/Game.exml'] = window.skins.AnimationSkin = (function (_super) {
	__extends(AnimationSkin, _super);
	function AnimationSkin() {
		_super.call(this);
		this.skinParts = ["player"];
		
		this.height = 600;
		this.width = 800;
		this.elementsContent = [this._Group1_i()];
	}
	var _proto = AnimationSkin.prototype;

	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.height = 200;
		t.width = 200;
		t.x = 100;
		t.y = 203;
		t.elementsContent = [this.player_i()];
		return t;
	};
	_proto.player_i = function () {
		var t = new eui.Rect();
		this.player = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xb51515;
		t.height = 86.67;
		t.width = 92.73;
		t.x = 67;
		t.y = 53.66;
		return t;
	};
	return AnimationSkin;
})(eui.Skin);generateEUI.paths['resource/scene/UIChrSel.exml'] = window.UIChrSelSkin = (function (_super) {
	__extends(UIChrSelSkin, _super);
	var UIChrSelSkin$Skin1 = 	(function (_super) {
		__extends(UIChrSelSkin$Skin1, _super);
		function UIChrSelSkin$Skin1() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_74")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UIChrSelSkin$Skin1.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UIChrSelSkin$Skin1;
	})(eui.Skin);

	var UIChrSelSkin$Skin2 = 	(function (_super) {
		__extends(UIChrSelSkin$Skin2, _super);
		function UIChrSelSkin$Skin2() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_75")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UIChrSelSkin$Skin2.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UIChrSelSkin$Skin2;
	})(eui.Skin);

	var UIChrSelSkin$Skin3 = 	(function (_super) {
		__extends(UIChrSelSkin$Skin3, _super);
		function UIChrSelSkin$Skin3() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_76")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UIChrSelSkin$Skin3.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UIChrSelSkin$Skin3;
	})(eui.Skin);

	var UIChrSelSkin$Skin4 = 	(function (_super) {
		__extends(UIChrSelSkin$Skin4, _super);
		function UIChrSelSkin$Skin4() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_77")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UIChrSelSkin$Skin4.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UIChrSelSkin$Skin4;
	})(eui.Skin);

	var UIChrSelSkin$Skin5 = 	(function (_super) {
		__extends(UIChrSelSkin$Skin5, _super);
		function UIChrSelSkin$Skin5() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_78")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UIChrSelSkin$Skin5.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UIChrSelSkin$Skin5;
	})(eui.Skin);

	var UIChrSelSkin$Skin6 = 	(function (_super) {
		__extends(UIChrSelSkin$Skin6, _super);
		function UIChrSelSkin$Skin6() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_62")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UIChrSelSkin$Skin6.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UIChrSelSkin$Skin6;
	})(eui.Skin);

	var UIChrSelSkin$Skin7 = 	(function (_super) {
		__extends(UIChrSelSkin$Skin7, _super);
		function UIChrSelSkin$Skin7() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_68")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UIChrSelSkin$Skin7.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UIChrSelSkin$Skin7;
	})(eui.Skin);

	function UIChrSelSkin() {
		_super.call(this);
		this.skinParts = ["i_chrsel","t_game_name","et_player_name","b_player_warrior","b_player_wizard","b_player_taoist","b_player_woman","b_player_man","b_player_sumbit","g_player_info","g_player_apparence","t_player_name1","t_player_job1","t_player_level1","t_player_name0","t_player_job0","t_player_level0","b_start","g_chrsel"];
		
		this.height = 600;
		this.width = 800;
		this.elementsContent = [this.g_chrsel_i()];
	}
	var _proto = UIChrSelSkin.prototype;

	_proto.g_chrsel_i = function () {
		var t = new eui.Group();
		this.g_chrsel = t;
		t.height = 600;
		t.touchEnabled = false;
		t.width = 800;
		t.x = 0;
		t.y = 0;
		t.elementsContent = [this.i_chrsel_i(),this.t_game_name_i(),this.g_player_info_i(),this.g_player_apparence_i(),this.t_player_name1_i(),this.t_player_job1_i(),this.t_player_level1_i(),this.t_player_name0_i(),this.t_player_job0_i(),this.t_player_level0_i(),this.b_start_i()];
		return t;
	};
	_proto.i_chrsel_i = function () {
		var t = new eui.Image();
		this.i_chrsel = t;
		t.source = "login_json.login_65";
		t.touchEnabled = false;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto.t_game_name_i = function () {
		var t = new eui.Label();
		this.t_game_name = t;
		t.bold = true;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.size = 16;
		t.text = "朔月传奇";
		t.textAlign = "center";
		t.top = 4;
		t.verticalAlign = "middle";
		return t;
	};
	_proto.g_player_info_i = function () {
		var t = new eui.Group();
		this.g_player_info = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 347;
		t.touchEnabled = false;
		t.width = 250;
		t.x = 103;
		t.y = 39.88;
		t.elementsContent = [this._Image1_i(),this.et_player_name_i(),this.b_player_warrior_i(),this.b_player_wizard_i(),this.b_player_taoist_i(),this.b_player_woman_i(),this.b_player_man_i(),this.b_player_sumbit_i()];
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.percentHeight = 100;
		t.source = "login_json.login_73";
		t.percentWidth = 100;
		t.y = 0;
		return t;
	};
	_proto.et_player_name_i = function () {
		var t = new eui.EditableText();
		this.et_player_name = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.fontFamily = "Microsoft YaHei";
		t.height = 12.17;
		t.size = 10;
		t.stroke = 1;
		t.strokeColor = 0xFCFCFC;
		t.text = "头号玩家1";
		t.textAlign = "center";
		t.textColor = 0x666666;
		t.width = 107.33;
		t.x = 62.33;
		t.y = 89.33;
		return t;
	};
	_proto.b_player_warrior_i = function () {
		var t = new eui.Button();
		this.b_player_warrior = t;
		t.height = 36;
		t.label = "";
		t.width = 44;
		t.x = 36.2;
		t.y = 127.4;
		t.skinName = UIChrSelSkin$Skin1;
		return t;
	};
	_proto.b_player_wizard_i = function () {
		var t = new eui.Button();
		this.b_player_wizard = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 36;
		t.label = "";
		t.width = 44;
		t.x = 73.4;
		t.y = 127.4;
		t.skinName = UIChrSelSkin$Skin2;
		return t;
	};
	_proto.b_player_taoist_i = function () {
		var t = new eui.Button();
		this.b_player_taoist = t;
		t.height = 36;
		t.label = "";
		t.width = 44;
		t.x = 111.8;
		t.y = 127.4;
		t.skinName = UIChrSelSkin$Skin3;
		return t;
	};
	_proto.b_player_woman_i = function () {
		var t = new eui.Button();
		this.b_player_woman = t;
		t.height = 36;
		t.label = "";
		t.width = 44;
		t.x = 73.4;
		t.y = 188.2;
		t.skinName = UIChrSelSkin$Skin4;
		return t;
	};
	_proto.b_player_man_i = function () {
		var t = new eui.Button();
		this.b_player_man = t;
		t.height = 36;
		t.label = "";
		t.width = 44;
		t.x = 111;
		t.y = 189;
		t.skinName = UIChrSelSkin$Skin5;
		return t;
	};
	_proto.b_player_sumbit_i = function () {
		var t = new eui.Button();
		this.b_player_sumbit = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 29.84;
		t.label = "";
		t.width = 63.5;
		t.x = 85.16;
		t.y = 297.5;
		t.skinName = UIChrSelSkin$Skin6;
		return t;
	};
	_proto.g_player_apparence_i = function () {
		var t = new eui.Group();
		this.g_player_apparence = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 347;
		t.touchEnabled = false;
		t.width = 250;
		t.x = 429;
		t.y = 38.88;
		return t;
	};
	_proto.t_player_name1_i = function () {
		var t = new eui.Label();
		this.t_player_name1 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.height = 13;
		t.size = 12;
		t.text = "unname";
		t.textColor = 0x998f8f;
		t.touchEnabled = false;
		t.width = 91.5;
		t.x = 115;
		t.y = 492.5;
		return t;
	};
	_proto.t_player_job1_i = function () {
		var t = new eui.Label();
		this.t_player_job1 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.height = 13;
		t.size = 12;
		t.text = "武士";
		t.textColor = 0x998F8F;
		t.touchEnabled = false;
		t.width = 91.5;
		t.x = 115;
		t.y = 552.5;
		return t;
	};
	_proto.t_player_level1_i = function () {
		var t = new eui.Label();
		this.t_player_level1 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.height = 13;
		t.size = 12;
		t.text = "0";
		t.textColor = 0x998F8F;
		t.touchEnabled = false;
		t.width = 91.5;
		t.x = 115;
		t.y = 524;
		return t;
	};
	_proto.t_player_name0_i = function () {
		var t = new eui.Label();
		this.t_player_name0 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.height = 13;
		t.size = 12;
		t.text = "unname";
		t.textColor = 0x998F8F;
		t.touchEnabled = false;
		t.width = 91.5;
		t.x = 668;
		t.y = 492.83;
		return t;
	};
	_proto.t_player_job0_i = function () {
		var t = new eui.Label();
		this.t_player_job0 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.height = 13;
		t.size = 12;
		t.text = "道士";
		t.textColor = 0x998F8F;
		t.touchEnabled = false;
		t.width = 91.5;
		t.x = 668;
		t.y = 552.83;
		return t;
	};
	_proto.t_player_level0_i = function () {
		var t = new eui.Label();
		this.t_player_level0 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.height = 13;
		t.size = 12;
		t.text = "0";
		t.textColor = 0x998F8F;
		t.touchEnabled = false;
		t.width = 91.5;
		t.x = 668;
		t.y = 524;
		return t;
	};
	_proto.b_start_i = function () {
		var t = new eui.Button();
		this.b_start = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 21;
		t.label = "";
		t.width = 44;
		t.x = 387;
		t.y = 455;
		t.skinName = UIChrSelSkin$Skin7;
		return t;
	};
	return UIChrSelSkin;
})(eui.Skin);generateEUI.paths['resource/scene/UILogin.exml'] = window.UILoginSkin = (function (_super) {
	__extends(UILoginSkin, _super);
	var UILoginSkin$Skin8 = 	(function (_super) {
		__extends(UILoginSkin$Skin8, _super);
		function UILoginSkin$Skin8() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_62")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UILoginSkin$Skin8.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UILoginSkin$Skin8;
	})(eui.Skin);

	var UILoginSkin$Skin9 = 	(function (_super) {
		__extends(UILoginSkin$Skin9, _super);
		function UILoginSkin$Skin9() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_61")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UILoginSkin$Skin9.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UILoginSkin$Skin9;
	})(eui.Skin);

	var UILoginSkin$Skin10 = 	(function (_super) {
		__extends(UILoginSkin$Skin10, _super);
		function UILoginSkin$Skin10() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_53")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UILoginSkin$Skin10.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UILoginSkin$Skin10;
	})(eui.Skin);

	var UILoginSkin$Skin11 = 	(function (_super) {
		__extends(UILoginSkin$Skin11, _super);
		function UILoginSkin$Skin11() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","sound_off_png")
					])
				,
				new eui.State ("disabled",
					[
						new eui.SetProperty("_Image1","source","sound_off_png")
					])
			];
		}
		var _proto = UILoginSkin$Skin11.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "sound_on_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UILoginSkin$Skin11;
	})(eui.Skin);

	var UILoginSkin$Skin12 = 	(function (_super) {
		__extends(UILoginSkin$Skin12, _super);
		function UILoginSkin$Skin12() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_52")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UILoginSkin$Skin12.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UILoginSkin$Skin12;
	})(eui.Skin);

	var UILoginSkin$Skin13 = 	(function (_super) {
		__extends(UILoginSkin$Skin13, _super);
		function UILoginSkin$Skin13() {
			_super.call(this);
			this.skinParts = ["labelDisplay"];
			
			this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
			this.states = [
				new eui.State ("up",
					[
					])
				,
				new eui.State ("down",
					[
						new eui.SetProperty("_Image1","source","login_json.login_62")
					])
				,
				new eui.State ("disabled",
					[
					])
			];
		}
		var _proto = UILoginSkin$Skin13.prototype;

		_proto._Image1_i = function () {
			var t = new eui.Image();
			this._Image1 = t;
			t.percentHeight = 100;
			t.source = "empty_png";
			t.percentWidth = 100;
			return t;
		};
		_proto.labelDisplay_i = function () {
			var t = new eui.Label();
			this.labelDisplay = t;
			t.horizontalCenter = 0;
			t.verticalCenter = 0;
			return t;
		};
		return UILoginSkin$Skin13;
	})(eui.Skin);

	function UILoginSkin() {
		_super.call(this);
		this.skinParts = ["i_bg","i_account_password","b_sumbit","b_reg","b_reg0","et_account","et_password","i_fg","c_sound","g_account_password","g_door_open","g_door","i_password_modify","g_password_modify","i_protocol","g_protocol","i_reg_input","b_reg_cancel","b_reg_sumbit","g_reg_input"];
		
		this.height = 600;
		this.width = 800;
		this.elementsContent = [this.g_account_password_i(),this.g_door_i(),this.g_password_modify_i(),this.g_protocol_i(),this.g_reg_input_i()];
	}
	var _proto = UILoginSkin.prototype;

	_proto.g_account_password_i = function () {
		var t = new eui.Group();
		this.g_account_password = t;
		t.height = 600;
		t.width = 800;
		t.x = 0;
		t.y = 0;
		t.layout = this._BasicLayout1_i();
		t.elementsContent = [this.i_bg_i(),this.i_account_password_i(),this.b_sumbit_i(),this.b_reg_i(),this.b_reg0_i(),this.et_account_i(),this.et_password_i(),this.i_fg_i(),this.c_sound_i()];
		return t;
	};
	_proto._BasicLayout1_i = function () {
		var t = new eui.BasicLayout();
		return t;
	};
	_proto.i_bg_i = function () {
		var t = new eui.Image();
		this.i_bg = t;
		t.blendMode = "add";
		t.percentHeight = 100;
		t.source = "bg_jpg";
		t.percentWidth = 100;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto.i_account_password_i = function () {
		var t = new eui.Image();
		this.i_account_password = t;
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "login_json.login_60";
		t.verticalCenter = 0;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto.b_sumbit_i = function () {
		var t = new eui.Button();
		this.b_sumbit = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 37.34;
		t.label = "";
		t.width = 79;
		t.x = 420.66;
		t.y = 334;
		t.skinName = UILoginSkin$Skin8;
		return t;
	};
	_proto.b_reg_i = function () {
		var t = new eui.Button();
		this.b_reg = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 35.67;
		t.label = "";
		t.width = 103;
		t.x = 273.66;
		t.y = 377;
		t.skinName = UILoginSkin$Skin9;
		return t;
	};
	_proto.b_reg0_i = function () {
		var t = new eui.Button();
		this.b_reg0 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 35;
		t.label = "";
		t.width = 132;
		t.x = 379.33;
		t.y = 377;
		t.skinName = UILoginSkin$Skin10;
		return t;
	};
	_proto.et_account_i = function () {
		var t = new eui.EditableText();
		this.et_account = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 15.34;
		t.size = 16;
		t.text = "";
		t.textAlign = "left";
		t.textColor = 0xb7b7b7;
		t.verticalAlign = "middle";
		t.width = 136;
		t.x = 351.33;
		t.y = 258.33;
		return t;
	};
	_proto.et_password_i = function () {
		var t = new eui.EditableText();
		this.et_password = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 15.34;
		t.size = 16;
		t.text = "";
		t.textAlign = "left";
		t.textColor = 0xB7B7B7;
		t.verticalAlign = "middle";
		t.width = 136;
		t.x = 350.66;
		t.y = 289.66;
		return t;
	};
	_proto.i_fg_i = function () {
		var t = new eui.Image();
		this.i_fg = t;
		t.height = 298;
		t.horizontalCenter = -181;
		t.source = "fg_png";
		t.verticalCenter = -151;
		t.width = 318;
		return t;
	};
	_proto.c_sound_i = function () {
		var t = new eui.CheckBox();
		this.c_sound = t;
		t.enabled = true;
		t.label = "";
		t.x = 750.33;
		t.y = 11.67;
		t.skinName = UILoginSkin$Skin11;
		return t;
	};
	_proto.g_door_i = function () {
		var t = new eui.Group();
		this.g_door = t;
		t.height = 600;
		t.visible = false;
		t.width = 800;
		t.x = 0;
		t.y = 0;
		t.layout = this._BasicLayout2_i();
		t.elementsContent = [this._Image1_i(),this.g_door_open_i()];
		return t;
	};
	_proto._BasicLayout2_i = function () {
		var t = new eui.BasicLayout();
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.source = "ChrSel_door_bg_png";
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto.g_door_open_i = function () {
		var t = new eui.Group();
		this.g_door_open = t;
		t.height = 362;
		t.width = 496;
		t.x = 152;
		t.y = 119;
		return t;
	};
	_proto.g_password_modify_i = function () {
		var t = new eui.Group();
		this.g_password_modify = t;
		t.height = 0;
		t.horizontalCenter = 0;
		t.verticalCenter = 0;
		t.visible = false;
		t.width = 0;
		t.elementsContent = [this.i_password_modify_i()];
		return t;
	};
	_proto.i_password_modify_i = function () {
		var t = new eui.Image();
		this.i_password_modify = t;
		t.horizontalCenter = 0;
		t.source = "login_json.login_50";
		t.verticalCenter = 0;
		return t;
	};
	_proto.g_protocol_i = function () {
		var t = new eui.Group();
		this.g_protocol = t;
		t.height = 0;
		t.horizontalCenter = 0;
		t.verticalCenter = 0;
		t.visible = false;
		t.width = 0;
		t.elementsContent = [this.i_protocol_i()];
		return t;
	};
	_proto.i_protocol_i = function () {
		var t = new eui.Image();
		this.i_protocol = t;
		t.horizontalCenter = 0;
		t.source = "login_json.login_1140";
		t.verticalCenter = 0;
		return t;
	};
	_proto.g_reg_input_i = function () {
		var t = new eui.Group();
		this.g_reg_input = t;
		t.percentHeight = 100;
		t.horizontalCenter = 0;
		t.verticalCenter = 0;
		t.visible = false;
		t.percentWidth = 100;
		t.elementsContent = [this.i_reg_input_i(),this.b_reg_cancel_i(),this.b_reg_sumbit_i()];
		return t;
	};
	_proto.i_reg_input_i = function () {
		var t = new eui.Image();
		this.i_reg_input = t;
		t.horizontalCenter = 0;
		t.source = "login_json.login_63";
		t.verticalCenter = 0;
		return t;
	};
	_proto.b_reg_cancel_i = function () {
		var t = new eui.Button();
		this.b_reg_cancel = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 37;
		t.label = "";
		t.width = 102.34;
		t.x = 525;
		t.y = 478;
		t.skinName = UILoginSkin$Skin12;
		return t;
	};
	_proto.b_reg_sumbit_i = function () {
		var t = new eui.Button();
		this.b_reg_sumbit = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 35.67;
		t.label = "";
		t.width = 79.67;
		t.x = 235;
		t.y = 478;
		t.skinName = UILoginSkin$Skin13;
		return t;
	};
	return UILoginSkin;
})(eui.Skin);
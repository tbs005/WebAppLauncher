var connect = require("connect");
var serveStatic = require("serve-static");

var app = connect();
app.use(serveStatic("package"));

app.listen(1337);

window.location = ("http://127.0.0.1:1337/");
console.log('Server running at http://127.0.0.1:1337/');
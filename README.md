### WebAppLauncher

利用nw.js, 打包html+js编写的Web应用, 使其可在mac/window/linux上双击执行(.exe)

#### demo

代码中的demo就是将 <http://fluttergo.com/PK/demo/index.html> 打包为本地桌面应用.

### GetStart

1. 克隆代码
2. 拷贝你打包的webApp的html,js,css等文件到./package目录下(没有该目录则创建)
3. 解压nwjs-*.zip到根目录,双击nw.exe启动app

ps :默认窗口大小800*600,可通过修改**package.json**修改

最终目录架构如下:

![1568716220316](README.assets/1568716220316.png)

### 原理

nw.js(内部集成有chrome+node.js) + 本地http静态资源服务器,运行html和js



### 优缺点

优点:打包简单,可选配置自动更新

缺点:打包体积过大.空包80M



### 补充

针对win平台,可以用zip压缩软件将零散的文件打包成一个自解压格式的exe,

解压后自动运行指定的exe.

具体如下图所示

![1568961758665](README.assets/1568961758665.png)

![1568961936999](README.assets/1568961936999.png)





### 其他

> 引用第三方组件
>
> nw.js
>
> <http://docs.nwjs.io/en/latest/References/Manifest%20Format/#window-subfields> 
>
> server-static
>
> https://github.com/expressjs/serve-static



